dhex (0.69-4) unstable; urgency=medium

  * debian/control:
      - Bumped Standards-Version to 4.6.0.
      - Changed my email in Maintainer field.
  * debian/copyright:
      - Changed my email in packaging block.
      - Updated some years.
  * debian/salsa-ci.yml: use Debian recipe for salsa CI.
  * debian/upstream/metadata: removed unknown Homepage field.

 -- Fabio Augusto De Muzio Tobich <ftobich@debian.org>  Tue, 19 Oct 2021 10:47:25 -0300

dhex (0.69-3) unstable; urgency=medium

  * debian/control: bumped Standards-Version to 4.5.1.

 -- Fabio Augusto De Muzio Tobich <ftobich@gmail.com>  Mon, 14 Dec 2020 09:54:27 -0300

dhex (0.69-2) unstable; urgency=medium

  * New maintainer. (Closes: #973686)
  * Using new DH level format. Consequently:
      - debian/compat: removed.
      - debian/control: changed from 'debhelper' to 'debhelper-compat' in
        Build-Depends field and bumped level to 13.
  * debian/control:
      - Added Rules-Requires-Root: no.
      - Added Vcs-* fields in source stanza.
      - Bumped Standards-Version to 4.5.0.
      - Changed Homepage field to use a secure URI.
  * debian/copyright:
      - Added myself and Axel Beckert to packaging block.
      - Added Upstream-Contact field.
      - Updated years in packaging block.
      - Using a secure URI in Format and Source fields.
  * debian/patches/:
      - 010_fix-typos.patch: added to fix several typos and spelling errors.
      - 020_hardening-fix.patch: added to be possible to implement hardening.
      - cpu-load: removed, does not work with 0.69 and causes buggy behavior.
        (Closes: #940536, LP: #1814478)
      - fix-typos: removed, was replaced by 010_fix-typos.patch.
      - harden: removed, it doesn't work in 0.69.
      - series: updated to reflect the changes.
  * debian/rules:
      - Removed unnecessary DEB_LDFLAGS_MAINT_APPEND flags.
      - Removed unnecessary DEB_CFLAGS_MAINT_APPEND flags.
  * debian/salsa-ci.yml: added to provide CI tests for Salsa.
  * debian/tests/:
      - control: created to perform a trivial CI test.
      - files/sample.txt: added to be used on tests.
  * debian/upstream/metadata: created.
  * debian/watch:
      - Bumped to version 4.
      - Updated to use a secure URI with HTTPS protocol.

 -- Fabio Augusto De Muzio Tobich <ftobich@gmail.com>  Mon, 09 Nov 2020 13:06:23 -0300

dhex (0.69-1) unstable; urgency=medium

  * New upstream version.
  * debian/control: drop Vcs fields.
  * Bump standards version to 4.2.1.
  * debian/copyright: updated years.
  * debian/install and manpages: added.
  * debian/rules: disabled install target.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Tue, 22 Jan 2019 08:06:27 +0100

dhex (0.68-3) unstable; urgency=medium

  * Add myself to Uploaders.
  * Drop stripping, thanks Helmut Grohne. (Closes: #865731)
  * Fix busy waiting for input. (Closes: #714014)
  * Bump debhelper version to 11.
  * Bump standards version to 4.1.3.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Wed, 14 Mar 2018 08:27:56 +0100

dhex (0.68-2) unstable; urgency=low

  * Upload to unstable because Wheezy is now stable. \o/

 -- Jonathan McCrohan <jmccrohan@gmail.com>  Mon, 06 May 2013 11:41:52 +0100

dhex (0.68-1) experimental; urgency=low

  * New upstream release
  * New maintainer (Closes: #691818)
    - Update debian/{control,copyright} accordingly.
  * Add debian/patches/dhex.1-man-typo to fix errors caught by lintian
  * Import packaging git repo to collab-maint
    - Add VCS-{Git,Browser} URLs
  * Bump S-V to 3.9.4
    - No changes needed
  * Fix Copyright Format 1.0 syntax error in debian/copyright
    - s/GNU GPL 2\+/GPL-2+/
  * Update package long description to aid searching
  * Thanks to Gregor Herrmann for reviewing and sponsoring this upload

 -- Jonathan McCrohan <jmccrohan@gmail.com>  Wed, 31 Oct 2012 22:38:35 +0000

dhex (0.67-1) unstable; urgency=low

  [ Gürkan Sengün ]
  * New upstream version. (Closes: #674782)
  * Bump standards version to 3.9.3.

  [ Axel Beckert ]
  * Enable hardening build flags
    + Bump debhelper compatibility to 9
    + Patch Makefile slightly by doing some s/=/+=/
  * Bump debian/copyright format to 1.0

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Fri, 22 Jun 2012 14:52:55 +0200

dhex (0.65-1) unstable; urgency=low

  * Initial release. (Closes: #613929)

 -- Gürkan Sengün <gurkan@phys.ethz.ch>  Thu, 27 Jan 2011 08:34:24 +0100
